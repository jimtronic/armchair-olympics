import fetch from 'node-fetch';
import fs from 'fs';
import http from 'http';

import cache from './cache';
import countriesByPerson from './data/people';
import weights from './data/weights';

const getPoints = async () => {
	if (cache.result) {
		return cache.result;
	}

	const response = await fetch('https://raw.githubusercontent.com/lsv/fifa-worldcup-2018/master/data.json')
	const worldCupResponse = await response.json()
	cache.teams = worldCupResponse.teams;
	cache.teams.forEach(team => team.weight = weights[team.fifaCode]);
	const points = Object.keys(weights).reduce((result, code) => { result[code] = 0; return result }, {});
	for (let group in worldCupResponse.groups) {
		worldCupResponse.groups[group].matches.forEach((match) => {
			if (match.finished) {
				if (match.home_result === match.away_result) {
					points[cache.teams[match.home_team - 1].fifaCode] += 0.5;
					points[cache.teams[match.away_team - 1].fifaCode] += 0.5;
				} else if (match.home_result > match.away_result) {
					points[cache.teams[match.home_team - 1].fifaCode] += 1;
				} else {
					points[cache.teams[match.away_team - 1].fifaCode] += 1;
				}
			}
		})
	}
	worldCupResponse.knockout.round_16.matches.forEach((match) => {
		if (match.winner) {
			points[cache.teams[match.away_team - 1].fifaCode] += 10;
		}
	})
	
	worldCupResponse.knockout.round_8.matches.forEach((match) => {
		if (match.winner) {
			points[cache.teams[match.away_team - 1].fifaCode] += 100;
		}
	})
	
	worldCupResponse.knockout.round_4.matches.forEach((match) => {
		if (match.winner) {
			points[cache.teams[match.away_team - 1].fifaCode] += 1000;
		}
	})
	
	worldCupResponse.knockout.round_2_loser.matches.forEach((match) => {
		if (match.winner) {
			points[cache.teams[match.away_team - 1].fifaCode] += 10000;
		}
	})
	
	worldCupResponse.knockout.round_2.matches.forEach((match) => {
		if (match.winner) {
			points[cache.teams[match.away_team - 1].fifaCode] += 100000;
		}
	})
	// example wins: { "JPN": 15600, "GER": 1232 }}
	const result = { lastUpdated: new Date(), points }
	cache.result = result
	setTimeout(() => delete cache.result, 1000 * 60 * 30)
	return result
}

const getStandings = async () => {
  const { lastUpdated, points } = await getPoints();
  
	const pointsPerPerson = Object.keys(countriesByPerson).reduce((pointspp, person) => {
		pointspp[person] = countriesByPerson[person].reduce((total, country) => 
			total + (points[country] * weights[country]), 0)
		return pointspp
	}, {})

	const standings = Object.keys(countriesByPerson)
		.sort((a, b) =>
			(pointsPerPerson[b] - pointsPerPerson[a]) || a.localeCompare(b))
		.map(person => ({
			person,
			score: pointsPerPerson[person],
			entries: countriesByPerson[person].sort().map(country => cache.teams.find(team => team.fifaCode === country))
		}));

	const rankingByWeight = Object.keys(weights).reduce((rankings, country) => {
		rankings.push({'name': [country][0],
			'point': points[country] * weights[country],
			'flag':  cache.teams.find(team => team.fifaCode === country).flag,
			'weight': weights[country],
			'medalCount': points[country]
		})
		return rankings;
	}, []).sort((a,b) => b.point - a.point);

	rankingByWeight.reduce((rank, country, index, rankings) => {
		country.rank = rank;
		return country.point === (rankings[index + 1] || {}).point ?
			rank :
			rank + 1
	}, 1);

	const maxPoint = rankingByWeight.reduce((max, country, index) => {
		index < 3 && (max += country.point);
		return max;
	}, 0);

	standings.reduce((rank, standing, index, standings) => {
		standing.rank = rank;
		return standing.score === (standings[index + 1] || {}).score ?
			rank :
			rank + 1
	}, 1);

	return { lastUpdated, standings, rankingByWeight, maxPoint }
};

const staticHtml = fs.readFileSync('./index.html');
const staticJs = fs.readFileSync('./client.js');
const staticCss = fs.readFileSync('./client.css');

const router = async (req, res) => {
	const map = {
		'/client.js': {
			head: { 'Content-Type': 'application/javascript' },
			data: () => staticJs
		},
		'/client.css': {
			head: { 'Content-Type': 'text/css' },
			data: () => staticCss
		},
		'/standings': {
			head: {
				'Content-Type': 'application/json',
				'Access-Control-Allow-Origin': '*'
			},
			data: async () => JSON.stringify(await getStandings())
		},
		'/medals': {
			head: {
				'Content-Type': 'application/json',
				'Access-Control-Allow-Origin': '*'
			},
			data: async () => JSON.stringify(await getPoints())
		}
	}

	const response = map[req.url] || {
		head: { 'Content-Type': 'text/html' },
		data: () => staticHtml
	}

	res.writeHead(200, response.head)
	res.end(await response.data())
}


http
	.createServer(router)
	.listen(process.env.PORT || 8080)
