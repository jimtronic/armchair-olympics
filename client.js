const getResults = async () => {
  const response = await fetch('/standings');
  return await response.json();
};

const getPoints = async () => {
  const response = await fetch('/medals');
  return await response.json();
};
 
var medals = {}

const createTableRow = ({
  rank,
  entries,
  name,
  score
}, head) => {
  const tr = document.createElement('tr');

  tr.appendChild(createTableCell(rank, head));
  if (Array.isArray(entries)) {
    const entriesCell = createEntriesCell(entries);
    tr.appendChild(entriesCell);
  } else {
    tr.appendChild(createTableCell(entries, head));
  }
  tr.appendChild(createTableCell(name, head));
  tr.appendChild(createTableCell(score || "0", head));
  return tr; 
}

const createEntriesCell = (entries) => {
  const td = document.createElement('td');
  entries.forEach (entry => {
    const img = new Image();
    img.src = entry.flag.replace(/[0-9]+px/, "40px");
    img.alt = entry.fullName + '[' + entry.weight +']';
    td.appendChild(img);
    const score = document.createElement('em');
    score.className = "medalScore";
    score.appendChild(document.createTextNode(medals.points[entry.fifaCode] + 'x' + entry.weight ));
    td.appendChild(score);
  });
  return td;
}
 
const createTableCell = (text, head) => {
  const td = document.createElement(head ? 'th' : 'td');
  td.appendChild(document.createTextNode(text));
  return td;
};

const getClass = rank => {
  return {
    '1': 'gold',
    '2': 'silver',
    '3': 'bronze'
  }[rank.toString()];
};

const renderTable = standings => {
  const existingTable = document.getElementsByTagName('table')[0];
  if (existingTable) {
    existingTable.remove()
  }

  const table = document.createElement('table');
  table.appendChild(createTableRow({
    name: 'Name',
    entries: '',
    rank: 'Rank',
    score: 'Score'
  }, true));

  standings
    .map(({ person, entries, rank, score }) => {
      const row = createTableRow({
        name: person,
        entries,
        rank,
        score
      });
      row.className = getClass(rank) || 'grey';
      return row;
    })
    .forEach(row => table.appendChild(row));
  document.body.appendChild(table);
};

const renderTime = lastUpdated => {
  const time = document.getElementsByTagName('time')[0];
  time.firstChild && time.removeChild(time.firstChild)
  time.appendChild(document.createTextNode(new Date(lastUpdated).toLocaleString()));
}

const renderMaxPoint = maxPoint => {
  const displayMaxPoint = document.getElementById('max-point');
  displayMaxPoint.appendChild(document.createTextNode(maxPoint));
}

const renderBestPicks = (rankings) => {
  const displayBestPicks = document.getElementById('best-pick');
  rankings.slice(0,3).forEach ((entry,index) => {
    const img = new Image();
    img.src = entry.flag.replace(/[0-9]+px/, "40px");
    img.alt = entry.name+ '[' + entry.point +']';
    displayBestPicks.appendChild(img);
    const score = document.createElement('em');
    score.className = "medalScore";
    score.appendChild(document.createTextNode(entry.medalCount + 'x' + entry.weight ));
    displayBestPicks.appendChild(score);
  });
  return displayBestPicks;
}

const render = ({
  lastUpdated,
  standings
}) => {
  renderTime(lastUpdated)
  renderTable(standings)
}

const entry = async () => {
  const results = await getResults();
  medals = await getPoints();
  renderMaxPoint(results.maxPoint);
  renderBestPicks(results.rankingByWeight);
  let prevUpdated = results.lastUpdated;
  render(results);

  setInterval(async () => {
    const results = await getResults();
    if (prevUpdated === results.lastUpdated) {
      return
    } else {
      prevUpdated = results.lastUpdated
    }

    render(results);
  }, 60000);

};

entry();
